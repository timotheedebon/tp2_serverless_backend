# Création de la file d'attente principale (job_offers_queue) avec redrive policy
resource "aws_sqs_queue" "job_offers_queue" {
  name                      = "job-offers-queue"
  delay_seconds             = 0
  max_message_size          = 256000
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.deadletter_queue.arn,
    maxReceiveCount = 4
  })
}

# Création de la file d'attente de lettres mortes (deadletter_queue)
resource "aws_sqs_queue" "deadletter_queue" {
  name = "deadletter-queue"
}



