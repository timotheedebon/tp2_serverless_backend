# resource aws_s3_bucket s3_job_offer_bucket
resource "aws_s3_bucket" "job_offers" {
  bucket = "nom-du-bucket-unique-timothee-debon-tp2-serverless-backend" 
  acl    = "private"  
}

# resource aws_s3_object job_offers
resource "aws_s3_bucket_object" "job_offers_directory" {
  bucket = aws_s3_bucket.job_offers.id
  key    = "job_offers/"
}

# resource aws_s3_bucket_notification bucket_notification

