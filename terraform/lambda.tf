data "archive_file" "empty_zip_code_lambda" {
  type        = "zip"
  output_path = "empty_lambda_code.zip"
  source {
    content  = "hello_world"
    filename = "dummy.txt"
  }
}

resource "aws_lambda_function" "s3_to_sqs_lambda" {
  function_name = "s3_to_sqs_lambda"
  handler = "index.handler"
  memory_size = 512
  timeout = 900
  runtime = "nodejs18.x"

  filename = "data.archive_file.empty_zip_code_lambda.output_path"  # Remplacez par le chemin correct

  role = aws_iam_role.lambda_execution_role.arn

  environment {
    variables = {
      QUEUE_URL = aws_sqs_queue.job_offers_queue.url
    }
  }

  source_code_hash = filebase64sha256("Chemin/vers/votre/code/index.zip")  # Remplacez par le chemin correct

}

# Assurez-vous que vous avez créé un rôle IAM pour cette fonction Lambda et référencez-le avec "aws_iam_role.lambda_execution_role" dans la configuration ci-dessus.





# resource aws_lambda_function s3_to_sqs_lambda


# resource aws_lambda_permission allow_bucket


# resource aws_lambda_function sqs_to_dynamo_lambda


# resource aws_lambda_permission allow_sqs_queue


# resource aws_lambda_function job_api_lambda



# resource aws_lambda_permission allow_api_gw

